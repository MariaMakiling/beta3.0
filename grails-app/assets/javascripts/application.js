// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require bower_components/jquery/dist/jquery.min.js
//= require bower_components/jquery-ui/jquery-ui.min.js
//= require bower_components/angular/angular.js
//= require_self
//= require bower_components/angular-resource/angular-resource.js
//= require bower_components/angular-ui-router/release/angular-ui-router.js
//= require bower_components/bootstrap-3.3.5-dist/js/bootstrap.min.js
//= require bower_components/a0-angular-storage/dist/angular-storage.js
//= require bower_components/angular-jwt/dist/angular-jwt.min.js
//= require bower_components/angular-route/angular-route.js

//= require bower_components/home/jquery-1.11.1.min.js
//= require bower_components/home/jquery-1.11.3.min.js
//= require bower_components/home/angular-cookies.min.js
//= require bower_components/home/ui-bootstrap-tpls-0.14.3.min.js

//= require bower_components/angular-poller/angular-poller.js
//= require bower_components/Chart.js/Chart.js
//= require bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js
//= require bower_components/rainbow/js/rainbow.min.js
//= require bower_components/rainbow/js/language/generic.js
//= require bower_components/rainbow/js/language/html.js
//= require bower_components/rainbow/js/language/javascript.js
//= require bower_components/angular-chart.js/angular-chart.js

//= require_tree app