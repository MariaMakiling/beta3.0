app.controller('Records', function($scope, diarrhearesults, bacterialinfectionresults, foodintakeresults, attachmentresults, immunizationresults, poller, store, $state, diarrheaOneFactory, bacterialInfectionFactory, foodIntakeFactory, attachmentFactory, immunizationOneFactory){

    $scope.diarrhearesults = diarrhearesults
    $scope.bacterialinfectionresults = bacterialinfectionresults
    //$scope.weightproblemresults = weightproblemresults
    $scope.foodintakeresults = foodintakeresults
    $scope.attachmentresults = attachmentresults
    $scope.immunizationresults = immunizationresults


    var diarrheaPoller = poller.get(diarrheaOneFactory, {
        action: 'query'
    })

    diarrheaPoller.promise.then(null, null, function(result){
        $scope.diarrhearesults = result
    })

    var bacterialinfectionPoller = poller.get(bacterialInfectionFactory, {
        action: 'query'
    })

    bacterialinfectionPoller.promise.then(null, null, function(result){
        $scope.bacterialinfectionresults = result
    })

    // var weightproblemPoller = poller.get(weightProblemFactory, {
    //     action: 'query'
    // })

    // weightproblemPoller.promise.then(null, null, function(result){
    //     $scope.weightproblemresults = result
    // })

    var foodintakePoller = poller.get(foodIntakeFactory, {
        action: 'query'
    })

    foodintakePoller.promise.then(null, null, function(result){
        $scope.foodintakeresults = result
    })

    var attachmentPoller = poller.get(attachmentFactory, {
        action: 'query'
    })

    attachmentPoller.promise.then(null, null, function(result){
        $scope.attachmentresults = result
    })

    var immunizationPoller = poller.get(immunizationOneFactory, {
        action: 'query'
    })

    immunizationPoller.promise.then(null, null, function(result){
        $scope.immunizationresults = result
    })
})