var app = angular.module('app', [
						'angular-jwt',
						'angular-storage',
						'ngResource',
						'ui.router',
						'chart.js',
						'emguo.poller'])
app.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'jwtInterceptorProvider', '$httpProvider', function($stateProvider, $locationProvider, $urlRouterProvider, jwtInterceptorProvider, $httpProvider){
	$urlRouterProvider.otherwise('/home')

	jwtInterceptorProvider.tokenGetter = function (store) {
		return store.get('jwt')
	}

	$httpProvider.interceptors.push('jwtInterceptor')

	$stateProvider
		.state("home", {
			url: "/home",
			templateUrl: "assets/app/partials/home.html",
			controller:'ImciOne',
			data: {
				requiresLogin: false
			}
		})
		.state("login", {
			url: "/login",
			templateUrl: "assets/app/partials/login.html",
			controller: 'Login',
			data: {
				requiresLogin: false
			}
		})
		.state("register", {
			url: "/register",
			templateUrl: "assets/app/partials/register.html",
			controller: 'Users',
			data: {
				requiresLogin: false
			}
		})
		.state("analytics", {
			url: "/analytics",
			templateUrl: "assets/app/partials/analytics.html",
			data: {
				requiresLogin: true
			}
		})
		.state("dictionary", {
			url: "/dictionary",
			templateUrl: "assets/app/partials/dictionary.html",
			controller: 'Dictionary',
			data: {
				requiresLogin: true
			}
		})
		.state("imci", {
			url: "/imci",
			templateUrl: "assets/app/partials/imci.html",
			controller: 'Imci',
			data: {
				requiresLogin: true
			}
		})
		.state("imcione", {
			url: "/imcione",
			templateUrl: "assets/app/partials/imciOne.html",
			// resolve: {
   //                          ImciOne: 'ImciOne',
   //                          imcioneresults: ['ImciOne', function (ImciOne) {
   //                              return ImciOne.query().$promise
   //                          }]
   //          },
            //controller: 'ImciOne',
			data: {
				requiresLogin: true
			}
		})
		.state("diarrheaone", {
			url: "/diarrheaone",
			templateUrl: "assets/app/partials/imci/one/diarrheaOne.html",
			controller: 'DiarrheaOne',
			data: {
				requiresLogin: true
			}
		})
		.state("bacterialinfection", {
			url: "/bacterialinfection",
			templateUrl: "assets/app/partials/imci/one/bacterialInfection.html",
			controller: 'BacterialInfection',
			data: {
				requiresLogin: true
			}
		})
		.state("foodintake", {
			url: "/foodintake",
			templateUrl: "assets/app/partials/imci/one/foodIntake.html",
			controller: 'FoodIntake',
			data: {
				requiresLogin: true
			}
		})
		.state("weightproblem", {
			url: "/weightproblem",
			templateUrl: "assets/app/partials/imci/one/weightProblem.html",
			controller: 'WeightProblem',
			data: {
				requiresLogin: true
			}
		})
		.state("attachment", {
			url: "/attachment",
			templateUrl: "assets/app/partials/imci/one/attachment.html",
			controller: 'Attachment',
			data: {
				requiresLogin: true
			}
		})
		.state("immunizationone", {
			url: "/immunizationone",
			templateUrl: "assets/app/partials/imci/one/immunizationOne.html",
			controller: 'ImmunizationOne',
			data: {
				requiresLogin: true
			}
		})
		.state("resultsone", {
			url: "/resultsone",
			templateUrl: "assets/app/partials/imci/one/resultsOne.html",
			controller: 'ImciOne',
			data: {
				requiresLogin: true
			}
		})
		.state("imcitwo", {
			url: "/imcitwo",
			templateUrl: "assets/app/partials/imciTwo.html",
			data: {
				requiresLogin: true
			}
		})
		.state("generaldangersigns", {
			url: "/generaldangersigns",
			templateUrl: "assets/app/partials/imci/two/generalDangerSigns.html",
			controller: 'GeneralDangerSigns',
			data: {
				requiresLogin: true
			}
		})
		.state("cough", {
			url: "/cough",
			templateUrl: "assets/app/partials/imci/two/cough.html",
			controller: 'Cough',
			data: {
				requiresLogin: true
			}
		})
		.state("diarrheatwo", {
			url: "/diarrheatwo",
			templateUrl: "assets/app/partials/imci/two/diarrheaTwo.html",
			controller: 'DiarrheaTwo',
			data: {
				requiresLogin: true
			}
		})
		.state("fever", {
			url: "/fever",
			templateUrl: "assets/app/partials/imci/two/fever.html",
			controller: 'Fever',
			data: {
				requiresLogin: true
			}
		})
		.state("measles", {
			url: "/measles",
			templateUrl: "assets/app/partials/imci/two/measles.html",
			controller: 'Measles',
			data: {
				requiresLogin: true
			}
		})
		.state("dengue", {
			url: "/dengue",
			templateUrl: "assets/app/partials/imci/two/dengue.html",
			controller: 'Dengue',
			data: {
				requiresLogin: true
			}
		})
		.state("earproblem", {
			url: "/earproblem",
			templateUrl: "assets/app/partials/imci/two/earProblem.html",
			controller: 'EarProblem',
			data: {
				requiresLogin: true
			}
		})
		.state("malnutritionanemia", {
			url: "/malnutritionanemia",
			templateUrl: "assets/app/partials/imci/two/malnutritionAnemia.html",
			controller: 'MalnutritionAnemia',
			data: {
				requiresLogin: true
			}
		})
		.state("immunizationtwo", {
			url: "/immunizationtwo",
			templateUrl: "assets/app/partials/imci/two/immunizationTwo.html",
			controller: 'ImmunizationTwo',
			data: {
				requiresLogin: true
			}
		})
		.state("vitamina", {
			url: "/vitamina",
			templateUrl: "assets/app/partials/imci/two/vitaminA.html",
			controller: 'VitaminA',
			data: {
				requiresLogin: true
			}
		})
		.state("records", {
			url: "/records",
			templateUrl: "assets/app/partials/records.html",
			resolve: {
                            diarrheaOneFactory: 'diarrheaOneFactory',
                            diarrhearesults: ['diarrheaOneFactory', function (diarrheaOneFactory) {
                                return diarrheaOneFactory.query().$promise
                            }],
                            bacterialInfectionFactory: 'bacterialInfectionFactory',
                            bacterialinfectionresults: ['bacterialInfectionFactory', function (bacterialInfectionFactory) {
                                return bacterialInfectionFactory.query().$promise
                            }],
                            // weightProblemFactory: 'weightProblemFactory',
                            // weightproblemresults: ['weightProblemFactory', function (weightProblemFactory) {
                            //     return weightProblemFactory.query().$promise
                            // }],
                            foodIntakeFactory: 'foodIntakeFactory',
                            foodintakeresults: ['foodIntakeFactory', function (foodIntakeFactory) {
                                return foodIntakeFactory.query().$promise
                            }],
                            attachmentFactory: 'attachmentFactory',
                            attachmentresults: ['attachmentFactory', function (attachmentFactory) {
                                return attachmentFactory.query().$promise
                            }],
                            immunizationOneFactory: 'immunizationOneFactory',
                            immunizationresults: ['immunizationOneFactory', function (immunizationOneFactory) {
                                return immunizationOneFactory.query().$promise
                            }],
                        },
            controller: 'Records',
			data: {
				requiresLogin: true
			}
		})
	}])

app.run(function ($rootScope, $state, store, jwtHelper, $stateParams){
	$rootScope.$state = $state
    $rootScope.$stateParams = $stateParams

    $rootScope.$on('$stateChangeStart', function (e, to) {
        if (to.data.requiresLogin) {

            if (!store.get('jwt') || jwtHelper.isTokenExpired(store.get('jwt'))) {
                e.preventDefault()
                $state.go('login')
            }
        }
    })
})