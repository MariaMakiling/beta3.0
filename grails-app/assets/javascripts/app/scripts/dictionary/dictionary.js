app.controller("Dictionary",function($scope){
    $scope.query = {};
    $scope.queryBy = '$';
    $scope.search = [
            {
                "term" : "Areola",
                "description" : "A small ring of color around a center portion, as about the nipple of the breast or the part of the iris surrounding the pupil of the eye. (American Heritage Dictionary of the English Language, Fifth Edition, Houghton Mifflin Harcourt Publishing Company, 2011)"
            },
            {
                "term" : "Fontanelle",
                "description" : "Any of the soft membranous gaps between the incompletely formed cranial bones of a fetus or an infant. Also called soft spot. (American Heritage Dictionary of the English Language, Fifth Edition, Houghton Mifflin Harcourt Publishing Company, 2011)"
            },
            {
                "term" : "Nasal flaring",
                "description" : "Nasal flaring is the enlargement of the opening of the nostrils during breathing. (Neil Kaneshiro, 2006) "
            },
            {
                "term" : "Pus",
                "description" : "hick, yellowish-white liquid that forms in infected body tissues. It consists mainly of dead white blood cells. (American Heritage Dictionary of Student Science, Second Edition,  Houghton Mifflin Harcourt Publishing Company, 2014)"
            },
            {
                "term" : "Pustule",
                "description" : "A small inflamed elevation of the skin that is filled with pus; a pimple. (American Heritage Dictionary of the English Language, Fifth Edition, Houghton Mifflin Harcourt Publishing Company, 2011)"
            },
            {
                "term" : "Umbilicus",
                "description" : "A small opening or depression similar to a navel, as the hollow at the base of the shell of some gastropod mollusks, one of the openings in the shaft of a feather, or the hilum of a seed. (American Heritage Dictionary of the English Language, Fifth Edition, Houghton Mifflin Harcourt Publishing Company, 2011)"
            },
            {
                "term" : "Anemia",
                "description" : "a quantitative deficiency of the hemoglobin, often accompanied by a reduced number of red blood cells and causing pallor, weakness, and breathlessness. (Random House Dictionary, Random House Inc., 2015)"
            },
            {
                "term" : "Capillary Refill",
                "description" : "the time taken for color to return to an external capillary bed after pressure is applied to cause blanching. (King D. Morton, 2013)"
            },
            {
                "term" : "Dengue",
                "description" : "an acute infectious disease caused by a flavivirus (species Dengue virus of the genus Flavivirus), transmitted by aedes mosquitoes, and characterized by headache, severe joint pain, and a rash — also called breakbone fever, dengue fever (Merriam-Webster Dictionary, 2015)"
            },
            {
                "term" : "Edema",
                "description" : "an abnormal infiltration and excess accumulation of serous fluid in connective tissue or in a serous cavity — called also dropsy. (Merriam-Webster Dictionary, 2015)"
            },
            {
                "term" : "Measles",
                "description" : "an acute infectious disease occurring mostly in children, characterized by catarrhal and febrile symptoms and an eruption of small red spots; rubeola. (Random House Dictionary, Random House Inc., 2015)"
            },
            {
                "term" : "Mouth Ulcer ",
                "description" : "also termed an oral ulcer, or a mucosal ulcer. An ulcer that occurs on the mucous membrane of the oral cavity. (Vorvick LJ, 2012)"
            },
            {
                "term" : "Pallor",
                "description" : "Extreme or unnatural paleness. (American Heritage Dictionary of the English Language, Fifth Edition, Houghton Mifflin Harcourt Publishing Company, 2011)"
            },
            {
                "term" : "Palmar",
                "description" : "of, relating to, or involving the palm of the hand. (Merriam-Webster Dictionary, 2015)"
            },
            {
                "term" : "Petechiae",
                "description" : "a minute, round, nonraised hemorrhage in the skin or in a mucous or serous membrane. (Random House Dictionary, Random House Inc., 2015)"
            },
            {
                "term" : "Stridor",
                "description" : "a harsh vibrating sound heard during respiration in cases of obstruction of the air passages. (Merriam-Webster Dictionary, 2015)"
            },
            {
                "term" : "Vomitus",
                "description" : "material ejected by vomiting. (Merriam-Webster Dictionary, 2015)"
            },
           
            

        ];
})