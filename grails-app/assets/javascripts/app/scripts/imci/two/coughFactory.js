app.factory('coughFactory', ['$resource', function ($resource) {
    return $resource('api/cough',
        {
            'update': {method: 'PUT'}
        })
    function getGeneralDangerSigns(drinkBreastfeed){
    	return $resource('api/generaldangersigns',
    	{
    		'get': {method: 'GET'}
    	})
    }
}])