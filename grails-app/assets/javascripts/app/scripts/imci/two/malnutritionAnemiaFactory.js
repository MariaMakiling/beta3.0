app.factory('malnutritionAnemiaFactory', ['$resource', function ($resource) {
    return $resource('api/malnutritionAnemia',
   		{
            'update': {method: 'PUT'}
        })
}])