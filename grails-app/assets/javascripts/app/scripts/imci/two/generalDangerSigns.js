app.controller('GeneralDangerSigns', function($scope, store, $state, generalDangerSignsFactory){

	$scope.generalDangerSigns = {}
	
		$scope.saveGeneralDangerSigns = function() {
			$scope.generalDangerSigns.assessment = $scope.getGeneralDangerSigns();
			console.log($scope.generalDangerSigns);
			generalDangerSignsFactory.save($scope.generalDangerSigns,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getGeneralDangerSigns = function() {
		var result = "";
		if ($scope.generalDangerSigns.drinkBreastfeed || $scope.generalDangerSigns.vomit
			|| $scope.generalDangerSigns.convulsion || $scope.generalDangerSigns.active){
			result = "has very severe disease";
		}else {
			result = "";
		}
		return result;
	};	
})