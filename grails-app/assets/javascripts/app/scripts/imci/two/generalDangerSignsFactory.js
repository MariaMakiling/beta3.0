app.factory('generalDangerSignsFactory', ['$resource', function ($resource) {
    return $resource('api/generaldangersigns',
        {
            'update': {method: 'PUT'}
        })
}])