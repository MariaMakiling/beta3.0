app.factory('earProblemFactory', ['$resource', function ($resource) {
    return $resource('api/earproblem',
        {
            'update': {method: 'PUT'}
        })
}])