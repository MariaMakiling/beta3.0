app.factory('imciTwoFactory', ['$resource', function ($resource) {
    return $resource('api/imcitwo',
        {
            'update': {method: 'PUT'}
        })
}])