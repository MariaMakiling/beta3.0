app.factory('diarrheaTwoFactory', ['$resource', function ($resource) {
    return $resource('api/diarrheatwo',
        {
            'update': {method: 'PUT'}
        })
}])