app.factory('immunizationTwoFactory', ['$resource', function ($resource) {
    return $resource('api/immunizationtwo',
        {
            'update': {method: 'PUT'}
        })
}])