app.controller('DiarrheaTwo', function($scope, store, $state, diarrheaTwoFactory){

	$scope.diarrheaTwo = {}
		$scope.saveDiarrheaTwo = function() {
			$scope.diarrheaTwo.assessment = $scope.getDiarrheaTwo();
			console.log($scope.diarrheaTwo);
			diarrheaTwoFactory.save($scope.diarrheaTwo,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getDiarrheaTwo = function() {
		var result = "";
		if ($scope.diarrheaTwo.diarrhea || $scope.diarrheaTwo.sleepy || $scope.diarrheaTwo.sunkenEyes && $scope.diarrheaTwo.skin > 2){
			result = "severe dehydration";
		}else if ($scope.diarrheaTwo.restlessIrritable || $scope.diarrheaTwo.sunkenEyes && $scope.diarrheaTwo.skin > 2){
			result = "some dehydration";
		}else if ($scope.diarrheaTwo.restlessIrritable || $scope.diarrheaTwo.sunkenEyes == false ){
			result = "some dehydration";
		}else if ($scope.diarrheaTwo.bloodStool){
			result = "dysentery";
		}else if ($scope.diarrheaTwo.diarrhea || $scope.diarrheaTwo.sleepy || $scope.diarrheaTwo.skin || $scope.diarrheaTwo.sunkenEyes
			&& $scope.diarrheaTwo.skin > 2 && $scope.diarrheaTwo.duration > 14){
			result ="severe persistent diarrhea";
		}else if ($scope.diarrheaTwo.diarrhea || $scope.diarrheaTwo.sleepy || $scope.diarrheaTwo.skin || $scope.diarrheaTwo.sunkenEyes && $scope.diarrheaTwo.skin > 2 && $scope.diarrheaTwo.duration > 14 ){
			result ="persistent diarrhea";
		} 
		return result;
	};	
})