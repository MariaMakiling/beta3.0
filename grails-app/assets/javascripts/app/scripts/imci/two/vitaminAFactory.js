app.factory('vitaminAFactory', ['$resource', function ($resource) {
    return $resource('api/vitaminA',
        {
            'update': {method: 'PUT'}
        })
}])