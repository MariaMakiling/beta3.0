app.factory('measlesFactory', ['$resource', function ($resource) {
    return $resource('api/measles',
        {
            'update': {method: 'PUT'}
        })
}])