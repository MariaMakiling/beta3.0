app.controller('Cough', function($scope, store, $state, coughFactory){

	$scope.cough = {}
		$scope.saveCough = function() {
			$scope.cough.assessment = $scope.getCough();
			console.log($scope.cough);
			coughFactory.save($scope.cough,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getCough = function() {
		var result = "";
		if ($scope.generalDangerSign.drinkBreastfeed || $scope.generalDangerSign.vomit
			|| $scope.generalDangerSign.convulsion || $scope.generalDangerSign.active || $scope.cough.stridor){
			result = "severe pneumonia has very severe disease";
		}else if ($scope.cough.chestIndrawing){
			result = "pneumonia";
		}else {
			result = "cough or cold";
		}
		return result;
	};	
})