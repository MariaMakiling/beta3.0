app.controller('EarProblem', function($scope, store, $state, earProblemFactory){
	$scope.earProblem = {}
		$scope.saveEarProblem = function() {
			$scope.earProblem.assessment = $scope.getEarProblem();
			console.log($scope.earProblem);
			earProblemFactory.save($scope.earProblem,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getEarProblem = function() {
		var result = "";
		if ($scope.earProblem.swelling){
			result = "mastoiditis";
		}else if ($scope.earProblem.discharge < 14 || $scope.earProblem.pain){
			result = "acute ear infection";
		}else if ($scope.earProblem.discharge < 14 || $scope.earProblem.pusDraining){
			result = "chronic ear infection";
		}else if ($scope.earProblem.pain && $scope.earProblem.pusDraining == false){
			result = "no ear infection";
		}
		return result;
	};	
})