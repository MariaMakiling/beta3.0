app.controller('Fever', function($scope, store, $state, feverFactory){
	$scope.fever = {}
		$scope.saveFever = function() {
			$scope.fever.assessment = getFever();
			console.log($scope.fever);
			feverFactory.save($scope.fever,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getFever = function() {
		if ($scope.generalDangerSigns.drinkBreastfeed || $scope.generalDangerSigns.vomit
			|| $scope.generalDangerSigns.convulsion || $scope.generalDangerSigns.active || $scope.fever.stiffNeck){
			return("very severe febrile disease");
		}else if ($scope.diarrhea.restlessIrritable || $scope.diarrhea.sunkenEyes){
			return("some dehydration");
		}else if ($scope.fever.measles && $scope.generalDangerSigns.drinkBreastfeed || $scope.generalDangerSigns.vomit
			|| $scope.generalDangerSigns.convulsion || $scope.generalDangerSigns.active || $scope.measles.deepExtensive || $scope.measles.cloudingCornea){
			return("severe complicated measles");
		}else if ($scope.fever.measles || $scope.measles.mouthUlcers|| $scope.measles.pusDraining){
			return("measles with eye or mouth complications");
		}else if ($scope.fever.measles = "yes"){
			return("measles");
		}else {
			return("fever");
		}
	};	
})