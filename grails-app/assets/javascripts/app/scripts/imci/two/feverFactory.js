app.factory('feverFactory', ['$resource', function ($resource) {
    return $resource('api/fever',
        {
            'update': {method: 'PUT'}
        })
}])