app.factory('weightProblemFactory', ['$resource', function ($resource) {
    return $resource('api/weightproblem',
        {
            'update': {method: 'PUT'}
        })
}])