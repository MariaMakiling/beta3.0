app.factory('attachmentFactory', ['$resource', function ($resource) {
    return $resource('api/attachment',
        {
            'update': {method: 'PUT'}
        })
}])