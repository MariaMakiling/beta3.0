app.controller('Imci', function($scope, store, $state, imciOneFactory, imciTwoFactory){
	$scope.imciOne = {}
	$scope.imciTwo = {}

		$scope.saveImciOne = function() {
			console.log($scope.imciOne);
			imciOneFactory.save($scope.imciOne,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

		$scope.saveImciTwo = function() {
			console.log($scope.imciTwo);
			imciTwoFactory.save($scope.imciTwo,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}
})