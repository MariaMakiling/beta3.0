app.factory('bacterialInfectionFactory', ['$resource', function ($resource) {
    return $resource('api/bacterialinfection',
        {
            'update': {method: 'PUT'}
        })
}])