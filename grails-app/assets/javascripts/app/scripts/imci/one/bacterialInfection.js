app.controller('BacterialInfection', function($scope, store, $state, bacterialInfectionFactory){

	$scope.bacterialInfection = {}

		$scope.saveBacterialInfection = function() {
			$scope.bacterialInfection.assessment = $scope.getBacterialInfection();
			console.log($scope.bacterialInfection);
			bacterialInfectionFactory.save($scope.bacterialInfection,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getBacterialInfection = function() {
		var result = "";
		if ($scope.bacterialInfection.feeding === true || $scope.bacterialInfection.convulsion === true || $scope.bacterialInfection.chestIndrawing === true 
			|| $scope.bacterialInfection.temperature === "38+°C" || $scope.bacterialInfection.movement === true){
			result = "very severe disease";
		}else if ($scope.bacterialInfection.umnbilicus === true && $scope.bacterialInfection.skinPustules === true ){
			result = "local bacterial infection";
		}else {
			result = "severe disease or local infection unlikely";
		}
		return result;
	};	
})