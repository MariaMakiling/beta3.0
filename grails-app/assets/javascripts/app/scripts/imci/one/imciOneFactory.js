app.factory('imciOneFactory', ['$resource', function ($resource) {
    return $resource('api/imcione',
        {
            'update': {method: 'PUT'}
        })
}])