app.factory('immunizationOneFactory', ['$resource', function ($resource) {
    return $resource('api/immunizationone',
        {
            'update': {method: 'PUT'}
        })
}])