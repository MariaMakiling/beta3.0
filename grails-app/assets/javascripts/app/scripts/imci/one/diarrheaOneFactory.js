app.factory('diarrheaOneFactory', ['$resource', function ($resource) {
    return $resource('api/diarrheaone',
        {
            'update': {method: 'PUT'}
        })
}])