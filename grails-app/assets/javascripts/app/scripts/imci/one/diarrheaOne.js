app.controller('DiarrheaOne', function($scope, store, $state, diarrheaOneFactory){
	
	$scope.diarrheaOne = {}
		$scope.saveDiarrheaOne = function() {
			$scope.diarrheaOne.assessment = $scope.getDiarrheaOne();
			// $scope.dirrheaOne.imciOne = 
			console.log($scope.diarrheaOne);
			diarrheaOneFactory.save($scope.diarrheaOne,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

		$scope.getDiarrheaOne = function() {
		var result = "";
		if ($scope.diarrheaOne.sunkenEyes === true && $scope.diarrheaOne.skin === "2+secs"){
			result = "severe dehydration";
		}else if ($scope.diarrheaOne.restlessIrritable === true || $scope.diarrheaOne.sunkenEyes === true && $scope.diarrheaOne.skin > 2){
			result = "some dehydration";
		}else {
			result = "no dehydration";
		}
		return result;
	};	
})