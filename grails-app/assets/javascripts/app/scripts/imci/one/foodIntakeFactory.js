app.factory('foodIntakeFactory', ['$resource', function ($resource) {
    return $resource('api/foodintake',
        {
            'update': {method: 'PUT'}
        })
}])