app.controller('FoodIntake', function($scope, store, $state, foodIntakeFactory){

	$scope.foodIntake = {}

		$scope.saveFoodIntake = function() {
			$scope.foodIntake.assessment = $scope.getFoodIntake();
			console.log($scope.foodIntake);
			foodIntakeFactory.save($scope.foodIntake,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getFoodIntake = function() {
		var result = "";
		if ($scope.foodIntake.other === true || $scope.foodIntake.breastfedCount === "0-8"){
			result = "feeding problem or low weight";
		}else {
			result = "no feeding problem";
		}
		return result;
	};	
})