app.factory('userFactory', ['$resource', function ($resource) {
    return $resource('api/users',
        {
            'update': {method: 'PUT'}
        })
}])