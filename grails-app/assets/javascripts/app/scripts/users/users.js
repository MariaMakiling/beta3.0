app.controller('Users', ['$scope', 'userFactory', '$state', function ($scope, userFactory, $state) {
    $scope.user = {}
    $scope.errors = []

    $scope.register = function () {
        $scope.errors = []
        userFactory.save($scope.user, function (result) {
            console.log(result)
            $state.go('login')
        }, function (error) {
            $scope.user = {}
            //for(var e in error.data.errors){
            //    $scope.errors = e
            //}
            console.log(error)
            for(var i = 0; i < error.data.errors.length; i++){
                $scope.errors.push(error.data.errors[i])
            }
        })
    }
}])