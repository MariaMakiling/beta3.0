package com.mariamakiling

class DiarrheaOne {

	boolean diarrhea
	String duration
	boolean bloodStool
	boolean sleepy
	boolean restlessIrritable
	boolean sunkenEyes
	String skin
	String assessment

	static belongsTo = [imciOne: ImciOne]

    static constraints = {
    	imciOne nullable: true
    }
}
