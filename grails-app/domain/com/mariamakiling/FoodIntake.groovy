package com.mariamakiling

class FoodIntake {

	boolean breastfed
	String breastfedCount
	boolean other
	String othersCount
	String milk
	String nightCount
	String dayCount
	String volume
	String preparation
	boolean breastMilk
	String food
	String cupBottle
	String cleaning
	String assessment

	static belongsTo = [imciOne: ImciOne]
	
    static constraints = {
    }
}
