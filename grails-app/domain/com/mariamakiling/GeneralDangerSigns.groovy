package com.mariamakiling

class GeneralDangerSigns {

	boolean drinkBreastfeed
	boolean vomit
	boolean convulsion
	boolean active
	String assessment
	
	static belongsTo = [imciTwo: ImciTwo]

    static constraints = {
    	imciTwo nullable: true
    }
}
