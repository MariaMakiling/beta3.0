package com.mariamakiling

class Admin extends User implements Serializable{

	static hasMany = [healthWorkers: HealthWorker]

    static constraints = {
    	healthWorkers nullable: true
    }
}
