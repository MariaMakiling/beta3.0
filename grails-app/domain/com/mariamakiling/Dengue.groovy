package com.mariamakiling

class Dengue {

	boolean bleeding
	boolean black
	boolean abdominalPain
	boolean vomiting
	boolean bleedingNoseGums
	boolean skinPetechiae
	boolean coldExtremities
	int capillaryRefill
	String tourniquetTest
	String assessment

	static belongsTo = [imciTwo: ImciTwo]
	
    static constraints = {
    }
}
