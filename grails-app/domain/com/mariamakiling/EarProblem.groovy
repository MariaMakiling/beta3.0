package com.mariamakiling

class EarProblem {

	boolean pain
	boolean discharge
	int duration
	boolean pusDraining
	boolean swelling
	String assessment

	static belongsTo = [imciTwo: ImciTwo]
	
    static constraints = {
    }
}
