package com.mariamakiling

class BacterialInfection {

	boolean feeding
	boolean convulsion
	String countBreaths
	boolean chestIndrawing
	boolean nasalFlaring
	boolean grunting
	boolean fontanelle
	String temperature
	boolean umbilicus
	boolean skinPustules
	boolean sleepy
	boolean movement
	String assessment

	static belongsTo = [imciOne: ImciOne]

    static constraints = {
    	imciOne nullable: true
    }
}
