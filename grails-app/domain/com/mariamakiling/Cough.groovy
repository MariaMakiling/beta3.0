package com.mariamakiling

class Cough {

	boolean cough
	int duration
	int count
	boolean chestIndrawing
	boolean stridor
	String assessment
	
	static belongsTo = [imciTwo: ImciTwo]

    static constraints = {
    	imciTwo nullable: true
    }
}
