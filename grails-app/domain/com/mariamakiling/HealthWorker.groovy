package com.mariamakiling

class HealthWorker extends User implements Serializable{

	static hasMany = [patients: Patient]
	
    static constraints = {
    	patients nullable: true, cascade: 'save-update'
    }
}
