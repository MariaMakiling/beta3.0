package com.mariamakiling

class Patient extends User implements Serializable{

	int weight
	String community
	String contactNumber

	static hasMany = [visits: PatientVisitation, monitors: PatientMonitoring, imciOne: ImciOne, imciTwo: ImciTwo]

    static constraints = {
    }
}
