package com.mariamakiling

class Fever {

	boolean fever
	String type
	int duration
	boolean everday
	boolean measles
	boolean stiffNeck
	boolean runnyNose
	boolean rash
	boolean measlesSymptoms
	String assessment

	static belongsTo = [imciTwo: ImciTwo]
	
    static constraints = {
    }
}
