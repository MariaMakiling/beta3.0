package com.mariamakiling

class OtherFood {

	boolean food
	int count
	double servings
	String useFeed
	boolean receive
	String who
	boolean change
	String how

	static belongsTo = [feedingProblem: FeedingProblem]

    static constraints = {
    }
}
