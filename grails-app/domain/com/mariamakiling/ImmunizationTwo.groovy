package com.mariamakiling

class ImmunizationTwo {

	boolean bcg
	boolean dpt1
	boolean dpt2
	boolean dpt3
	boolean opv1
	boolean opv2
	boolean opv3
	boolean hepb1
	boolean hepb2
	boolean hepb3
	boolean measles

	static belongsTo = [imciTwo: ImciTwo]

    static constraints = {
    }
}
