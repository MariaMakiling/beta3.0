package com.mariamakiling

class FeedingProblem {

	boolean breastfed
	int duration
	boolean night
	boolean others

	static belongsTo = [imciTwo: ImciTwo]
	static hasOne = [otherFood: OtherFood]

    static constraints = {
    }
}
