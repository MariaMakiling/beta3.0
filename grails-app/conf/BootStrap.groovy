import com.mariamakiling.*

class BootStrap {

    def init = { servletContext ->

        //user bootstrap
    	def admin = new Admin(name: "Zarraga", username: "zarraga", password: "password", address: "Zarraga, Iloilo")
    	def healthWorker = new HealthWorker(name: "Joy", age: 34, gender: "Female", address: "Zarraga, Iloilo", username: "joy", password: "password")
    	def patient1 = new Patient(name: "Toto", age: 2, gender: "Male", address: "Zarraga, Iloilo", username: "toto", password: "password", weight: 46, community: "Zarraga", contactNumber: "09123456789")
    	def patient2 = new Patient(name: "Nene", age: 1, gender: "Female", address: "Zarraga, Iloilo", username: "nene", password: "password", weight: 40, community: "Zarraga", contactNumber: "09987654321")

    	admin.save()
    	healthWorker.save()
    	patient1.save()
    	patient2.save()

        //imci1 bootstrap
        def weightProblem = new WeightProblem(lowWeight: false, trush: false)
        def immunizationOne = new ImmunizationOne(bcg: true, dpt1: true, opv1: false, hepb1: false)
        def attachment = new Attachment(chin: false, mouth: true, lip: false, areola: true, sucking: "effective")
        def diarrheaOne = new DiarrheaOne(diarrhea: false, duration: 3, bloodStool: true, sleepy: false, restlessIrritable: false, sunkenEyes: true, skin: 1.4)
        def bacterialInfection = new BacterialInfection(feeding: true, convulsion: false, countBreaths: 23, chestIndrawing: false, nasalFlaring: false, grunting: false, fontanelle: false, temperature: 36.5, umbillicus: true, skinPustules: false, sleepy: true, movement: false)
        def foodIntake = new FoodIntake(breastfed: true)
        def breastfed = new Breastfed(breastfed: true, breastfedCount: 5, other: false, othersCount: 4)
        def notBreastfed = new NotBreastfed(milk: "Nido", nightCount: 2, dayCount: 3, volume: 15, preparation: "prepare", breastMilk: true, food: "food", cupBottle: "cup", cleaning: "clean")

        weightProblem.save()
        immunizationOne.save()
        attachment.save()
        diarrheaOne.save()
        bacterialInfection.save()
        foodIntake.save()
        breastfed.save()
        notBreastfed.save()

        //imci2 bootstrap
        def generalDangerSigns = new GeneralDangerSigns(drinkBreastfeed: true, vomit: true, convulsion: false, active: true)
        def cough = new Cough(cough: true, duration: 3, count: 10, chestIndrawing: false, stridor: false)
        def measles = new Measles(mouthUlcers: false, deepExtensive: false, pusDraining: true, cloudingCornea: false)
        def dengue = new Dengue(bleeding: false, black:false, abdominalPain: true, vomiting: false, bleedingNoseGums: false, skinPetechiae:false, coldExtremities: true, capillaryRefill:5, tourniquetTest: "DNF unlikely")
        def fever = new Fever(fever: true, type: "low malaria risk", duration: 4, everday: false, measles: false, stiffNeck: true, runnyNose: false, rash: true, measlesSymptoms: true)
        def vitaminA = new VitaminA(sixMos: true, receive: true)
        def earProblem = new EarProblem(pain: false, discharge: false, duration: 0, pusDraining: true, swelling: true)
        def malnutritionAnemia = new MalnutritionAnemia(wasting: false, edemaFeet: false, palmarPallor: false, lowWeight: false)
        def feedingProblem = new FeedingProblem(breastfed: true, duration: 3, night: true, others: true)
        def immunizationTwo = new ImmunizationTwo(bcg: true, dpt1: true, dpt2: true, dpt3: true, opv1: true, opv2: true, opv3: false, hepb1: true, hepb2: true, hepb3: true, measles: true)

        generalDangerSigns.save()
        cough.save()
        measles.save()
        dengue.save()
        fever.save()
        vitaminA.save()
        earProblem.save()
        malnutritionAnemia.save()
        feedingProblem.save()
        immunizationTwo.save()

        //user roles
    	def roleAdmin = new Role(authority: "ROLE_ADMIN")
    	def roleHealthWorker = new Role(authority: "ROLE_HEALTH_WORKER")
    	def rolePatient = new Role(authority: "ROLE_PATIENT")

    	roleAdmin.save()
    	roleHealthWorker.save()
    	rolePatient.save()

    	admin.addToHealthWorkers(healthWorker)
    	healthWorker.addToPatients(patient1)
    	healthWorker.addToPatients(patient2)

        //users for debugging
    	println(admin.gender)
    	println(healthWorker)
    	println(patient1)
    	println(patient2)
    	println(rolePatient)
    	println(roleAdmin)
    	println(roleHealthWorker)

        //imci1 for debugging
        println(weightProblem.lowWeight)
        println(immunizationOne.bcg)
        println(attachment.chin)
        println(diarrheaOne.duration)
        println(bacterialInfection.feeding)
        println(foodIntake.breastfed)
        println(breastfed.breastfedCount)
        println(notBreastfed.milk)

        //imci2 for debugging
        println(generalDangerSigns.vomit)
        println(cough.duration)
        println(measles.pusDraining)
        println(dengue.tourniquetTest)
        println(fever.type)
        println(vitaminA.receive)
        println(earProblem.duration)
        println(malnutritionAnemia.wasting)
        println(feedingProblem.duration)
        println(immunizationTwo.measles)

    	UserRole.create(admin, roleAdmin, true)
    	UserRole.create(healthWorker, roleHealthWorker, true)
    	UserRole.create(patient1, rolePatient, true)
    	UserRole.create(patient2, rolePatient, true)

    	//new UserRole(user: admin, role: roleAdmin).save()

    }
    def destroy = {
    }
}
