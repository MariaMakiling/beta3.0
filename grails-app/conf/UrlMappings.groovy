class UrlMappings {

	static mappings = {

        "/"(view:"/index")
        "500"(view:'/error')

        //listAll
        "/api/imcione/list"(controller: 'imciOne', action: 'list')
        "/api/diarrheaone/list"(controller: 'diarrheaOne', action: 'list')
        "/api/bacterialinfection/list"(controller: 'bacterialInfection', action: 'list')
        "/api/weightproblem/list"(controller: 'weightProblem', action: 'list')
        "/api/foodintake/list"(controller: 'foodIntake', action: 'list')
        "/api/attachment/list"(controller: 'attachment', action: 'list')
        "/api/immunizationone/list"(controller: 'immunizationOne', action: 'list')

        "/api/generaldangersigns/list"(controller: 'generalDangerSigns', action: 'list')
        "/api/cough/list"(controller: 'cough', action: 'list')
        "/api/diarrheatwo/list"(controller: 'diarrheaTwo', action: 'list')
        "/api/measles/list"(controller: 'measles', action: 'list')
        "/api/dengue/list"(controller: 'dengue', action: 'list')
        "/api/fever/list"(controller: 'fever', action: 'list')
        "/api/earproblem/list"(controller: 'earProblem', action: 'list')
        "/api/malnutritionanemia/list"(controller: 'malnutritionAnemia', action: 'list')
        "/api/feedingproblem/list"(controller: 'feedingProblem', action: 'list')
        "/api/immunizationtwo/list"(controller: 'immunizationTwo', action: 'list')
        "/api/vitamina/list"(controller: 'vitaminA', action: 'list')

        //users
        "/api/users"(resources: 'users')
        "/api/patients"(resources: 'patient')
        "/api/healthworkers"(resources: 'healthWorker')
        "/api/admins"(resources: 'admin')

        //imci1
        "/api/imcione" (resources: 'imciOne')
        "/api/weightproblem" (resources: 'weightProblem')
        "/api/immunizationone" (resources: 'immunizationOne')
        "/api/attachment" (resources: 'attachment')
        "/api/diarrheaone" (resources: 'diarrheaOne')
        "/api/bacterialinfection" (resources: 'bacterialInfection')
        "/api/foodintake" (resources: 'foodIntake')
        "/api/breastfed" (resources: 'breastfed')
        "/api/notbreastfed" (resources: 'notBreastfed')

        //imci2
        "/api/imcitwo" (resources: 'imciTwo')
        "/api/generaldangersigns" (resources: 'generalDangerSigns')
        "/api/cough" (resources: 'cough')
        "/api/diarrheatwo" (resources: 'diarrheaTwo')
        "/api/measles" (resources: 'measles')
        "/api/dengue" (resources: 'dengue')
        "/api/fever" (resources: 'fever')
        "/api/earproblem" (resources: 'earProblem')
        "/api/malnutritionanemia" (resources: 'malnutritionAnemia')
        "/api/feedingproblem" (resources: 'feedingProblem')
        "/api/immunizationtwo" (resources: 'immunizationTwo')
        "/api/vitamina" (resources: 'vitaminA')

	}
}
