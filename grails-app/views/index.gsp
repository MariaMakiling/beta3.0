<!DOCTYPE html>
<html ng-app="app">
<head>
  <title>Maria Makiling</title>
  <asset:javascript src="application.js"/>
  <asset:stylesheet src="application.css"/>

</head>
<body>
  <base href="/mariamakiling/" />
  <div ng-include="assets/app/partials/layout/header.html"></div>

     
      <!-- Start of Header Navigation Bar -->
      <div class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" ui-sref="home">Maria Makiling</a>
          </div>
          <div class="navbar-collapse collapse" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li><a ui-sref="home"><span class="fa fa-dashboard"></span> &nbsp; Dashboard</a></li>
              <li><a ui-sref="imci"><span class="fa fa-child"></span> &nbsp; IMCI</a></li>
              <li><a ui-sref="records"><span class="fa fa-file-text"></span> &nbsp; Records</a></li>
              <li><a ui-sref="analytics"><span class="fa fa-line-chart"></span> &nbsp; Analytics</a></li>
              <li><a ui-sref="dictionary"><span class="fa fa-book"></span> &nbsp; Dictionary</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>
    </div> <!-- /container -->
     <!-- End of Header Navigation Bar -->

  
  <div ui-view></div>

  <a ui-sref="footer"></a>
</body>
</html>