package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class FeedingProblemController {

    def index() { 
    	render FeedingProblem.list() as JSON
    }

    def save() {
    	def newFeedingProblem= new FeedingProblem(request.JSON)
    	newFeedingProblem.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def feedingProblem = FeedingProblem.get(params.id)

    	render feedingProblem as JSON
    }
}
