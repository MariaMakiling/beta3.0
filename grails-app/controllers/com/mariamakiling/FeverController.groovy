package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class FeverController {

    def index() { 
    	render Fever.list() as JSON
    }

    def save() {
    	def newFever = new Fever(request.JSON)
    	newFever.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def fever = Fever.get(params.id)

    	render fever as JSON
    }

    def list(){
        def listAllFever = Fever.executeQuery("from Fever")
        StringWriter outputWriter = new StringWriter()

        listAllFever.each { fever ->
            outputWriter.write("${fever.fever},${fever.type},${fever.duration},${fever.everyday},${fever.measles},${fever.stiffNeck},${fever.runnyNose},${fever.rash},${fever.measlesSymptoms},${fever.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
