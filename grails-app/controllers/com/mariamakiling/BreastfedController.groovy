package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class BreastfedController {

    def index() {
    	render Breastfed.list() as JSON
    }

    def save() {
    	def newBreastfed = new Breastfed(request.JSON)
    	newBreastfed.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def breastfed = Breastfed.get(params.id)

    	render breastfed as JSON
    }
}
