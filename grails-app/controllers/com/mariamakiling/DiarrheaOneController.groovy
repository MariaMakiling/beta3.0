package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class DiarrheaOneController {

    def index() {
    	render DiarrheaOne.list() as JSON
    }

    def save() {
    	def newDiarrheaOne = new DiarrheaOne(request.JSON)
        def imciOneList = ImciOne.list()
        def imciOne = imciOneList.last()
        newDiarrheaOne.imciOne = imciOne
    	newDiarrheaOne.save(failOnError:true)
        println request.JSON
        println newDiarrheaOne
    	render(['success': true] as JSON)
    }

    def show() {
    	def diarrheaOne = DiarrheaOne.get(params.id)

    	render diarrheaOne as JSON
    }

    def list(){
        def listAllDiarrheaOne = DiarrheaOne.executeQuery("from DiarrheaOne")
        StringWriter outputWriter = new StringWriter()

        listAllDiarrheaOne.each { diarrheaOne ->
            outputWriter.write("diarrhea:${diarrheaOne.diarrhea}, duration:${diarrheaOne.duration}, bloodStool:${diarrheaOne.bloodStool}, sleepy:${diarrheaOne.sleepy}, restlessIrritable:${diarrheaOne.restlessIrritable}, sunkenEyes:${diarrheaOne.sunkenEyes}, skin:${diarrheaOne.skin}, assessment:${diarrheaOne.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
