package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class HealthWorkerController {

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index() {
		render HealthWorker.list() as JSON
	}

	def save() {
		def healthWorker = new HealthWorker(request.JSON)
		if(healthWorker.validate()){
			healthWorker.save()
			healthWorker.assignRole("ROLE_HEALTH_WORKER")
			render(["success":true] as JSON)
		} else {
			response.status = 500
			render(healthWorker.errors as JSON)
		}
	}

	def show() {
		println(params.id)
		render HealthWorker.get(params.id) as JSON
	}

	def update() {
		def healthWorker = HealthWorker.get(params.id)
		healthWorker.setProperties(request.JSON)
		if(healthWorker.validate()){
			healthWorker.save()
			render(["success":true] as JSON)
		} else {
			response.status = 500
			render(healthWorker.errors as JSON)
		}
	}

	def delete() {
		def healthWorker = HealthWorker.get(params.id)
		UserRole.remove(healthWorker, Role.findByAuthority("ROLE_HEALTH_WORKER"), true)
		healthWorker.delete(flush: true)
		render(["success":true] as JSON)
	}
}
