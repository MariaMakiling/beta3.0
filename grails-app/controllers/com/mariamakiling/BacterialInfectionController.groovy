package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class BacterialInfectionController {

    def index() { 
    	render BacterialInfection.list() as JSON
    }

    def save() {
    	def newBacterialInfection = new BacterialInfection(request.JSON)
        def imciOneList = ImciOne.list()
        def imciOne = imciOneList.last()
    	newBacterialInfection.imciOne = imciOne
        newBacterialInfection.save(failOnError:true)
        println request.JSON
        println newBacterialInfection
        render(['success': true] as JSON)
    }

    def show() {
    	def bacterialInfection = BacterialInfection.get(params.id)

    	render bacterialInfection as JSON
    }

    def list(){
        def listAllBacterialInfection = BacterialInfection.executeQuery("from BacterialInfection")
        StringWriter outputWriter = new StringWriter()

        listAllBacterialInfection.each { bacterialInfection ->
            outputWriter.write("feeding:${bacterialInfection.feeding}, convulsion:${bacterialInfection.convulsion}, countBreaths:${bacterialInfection.countBreaths}, chestIndrawing:${bacterialInfection.chestIndrawing}, nasalFlaring:${bacterialInfection.nasalFlaring}, grunting:${bacterialInfection.grunting}, fontanelle:${bacterialInfection.fontanelle}, temperature:${bacterialInfection.temperature}, umbilicus:${bacterialInfection.umbilicus}, skinPustules:${bacterialInfection.skinPustules}, sleepy:${bacterialInfection.sleepy}, movement:${bacterialInfection.movement}, assessment:${bacterialInfection.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
