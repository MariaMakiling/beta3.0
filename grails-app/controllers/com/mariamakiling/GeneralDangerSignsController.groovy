package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class GeneralDangerSignsController {

    def index() { 
    	render GeneralDangerSigns.list() as JSON
    }

    def save() {
    	def newGeneralDangerSigns = new GeneralDangerSigns(request.JSON)
        def imciTwoList = ImciTwo.list()
        def imciTwo = imciTwoList.last()
        newGeneralDangerSigns.imciTwo = imciTwo
        newGeneralDangerSigns.save(failOnError:true)
        println request.JSON
        println newGeneralDangerSigns
        render(['success': true] as JSON)
    }

    def show() {
    	def generalDangerSigns = GeneralDangerSigns.get(params.id)

    	render generalDangerSigns as JSON
    }

    def list(){
        def listAllGeneralDangerSigns = GeneralDangerSigns.executeQuery("from GeneralDangerSigns")
        StringWriter outputWriter = new StringWriter()

        listAllGeneralDangerSigns.each { generalDangerSigns ->
            outputWriter.write("${generalDangerSigns.drinkBreastfeed},${generalDangerSigns.vomit},${generalDangerSigns.convulsion},${generalDangerSigns.active},${generalDangerSigns.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
