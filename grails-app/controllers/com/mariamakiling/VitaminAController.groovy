package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class VitaminAController {

    def index() { 
    	render VitaminA.list() as JSON
    }

    def save() {
    	def newVitaminA= new VitaminA(request.JSON)
    	newVitaminA.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def vitaminA = VitaminA.get(params.id)

    	render vitaminA as JSON
    }
}
