package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class FoodIntakeController {

    def index() { 
    	render FoodIntake.list() as JSON
    }

    def save() {
    	def newFoodIntake = new FoodIntake(request.JSON)
        def imciOneList = ImciOne.list()
        def imciOne = imciOneList.last()
        newFoodIntake.imciOne = imciOne
        newFoodIntake.save(failOnError:true)
        println request.JSON
        println newFoodIntake
        render(['success': true] as JSON)
    }

    def show() {
    	def foodIntake = FoodIntake.get(params.id)

    	render foodIntake as JSON
    }

    def list(){
        def listAllFoodIntake = FoodIntake.executeQuery("from FoodIntake")
        StringWriter outputWriter = new StringWriter()

        listAllFoodIntake.each { foodIntake ->
            outputWriter.write("breastfed:${foodIntake.breastfed}, breastfedCount:${foodIntake.breastfedCount}, other:${foodIntake.other}, othersCount:${foodIntake.othersCount}, milk:${foodIntake.milk}, nightCount:${foodIntake.nightCount}, dayCount:${foodIntake.dayCount}, volume:${foodIntake.volume}, preparation:${foodIntake.preparation}, breastMilk:${foodIntake.breastMilk}, food:${foodIntake.food}, cupBottle:${foodIntake.cupBottle},cleaning:${foodIntake.cleaning},assessment:${foodIntake.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
