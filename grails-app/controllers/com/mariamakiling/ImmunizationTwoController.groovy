package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class ImmunizationTwoController {

	static allowedMethods = [save: "POST"]

    def index() { 
    	render ImmunizationTwo.list as JSON
    }

    def save() {
    	def newImmunizationTwo = new ImmunizationTwo(request.JSON)
        def imciTwoList = ImciOne.list()
        def imciTwo = imciTwoList.last()
        newImmunizationTwo.imciTwo = imciTwo
        newImmunizationTwo.save(failOnError:true)
        println request.JSON
        println newImmunizationTwo
        render(['success': true] as JSON)
    }

    def show() {
    	def immunizationTwo = ImmunizationTwo.get(params.id)

    	render immunizationTwo as JSON
    }

    def list(){
        def listAllImmunizationTwo = ImmunizationTwo.executeQuery("from ImmunizationTwo")
        StringWriter outputWriter = new StringWriter()

        listAllImmunizationTwo.each { immunizationTwo ->
            outputWriter.write("${immunizationTwo.bcg},${immunizationTwo.dpt1},${immunizationTwo.dpt2},${immunizationTwo.dpt3},${immunizationTwo.opv1},${immunizationTwo.opv2},${immunizationTwo.opv3},${immunizationTwo.hepb1},${immunizationTwo.hepb2},${immunizationTwo.hepb3},${immunizationTwo.measles}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
