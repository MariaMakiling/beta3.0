package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class AttachmentController {

    def index() { 
    	render Attachment.list() as JSON
    }

    def save() {
    	def newAttachment = new Attachment(request.JSON)
        def imciOneList = ImciOne.list()
        def imciOne = imciOneList.last()
        newAttachment.imciOne = imciOne
        newAttachment.save(failOnError:true)
        println request.JSON
        println newAttachment
        render(['success': true] as JSON)
    }

    def show() {
    	def attachment = Attachment.get(params.id)

    	render attachment as JSON
    }

    def list(){
        def listAllAttachment = Attachment.executeQuery("from Attachment")
        StringWriter outputWriter = new StringWriter()

        listAllAttachment.each { attachment ->
            outputWriter.write("chin:${attachment.chin}, mouth:${attachment.mouth}, lip:${attachment.lip}, areola:${attachment.areola}, sucking:${attachment.sucking}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
