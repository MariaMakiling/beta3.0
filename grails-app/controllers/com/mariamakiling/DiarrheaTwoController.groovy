package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class DiarrheaTwoController {

    def index() {
    	render DiarrheaTwo.list() as JSON
    }

    def save() {
    	def newDiarrheaTwo = new DiarrheaTwo(request.JSON)
        def imciTwoList = ImciTwo.list()
        def imciTwo = imciTwoList.last()
        newDiarrheaTwo.imciTwo = imciTwo
        newDiarrheaTwo.save(failOnError:true)
        println request.JSON
        println newDiarrheaTwo
        render(['success': true] as JSON)
    }

    def show() {
    	def diarrheaTwo = DiarrheaTwo.get(params.id)

    	render diarrheaTwo as JSON
    }

    def list(){
        def listAllDiarrheaTwo = DiarrheaTwo.executeQuery("from DiarrheaTwo")
        StringWriter outputWriter = new StringWriter()

        listAllDiarrheaTwo.each { diarrheaTwo ->
            outputWriter.write("${diarrheaTwo.diarrhea},${diarrheaTwo.duration},${diarrheaTwo.bloodStool},${diarrheaTwo.sleepy},${diarrheaTwo.restlessIrritable},${diarrheaTwo.unableDrink},${diarrheaTwo.unableDrink},${diarrheaTwo.drinkingEagerly},${diarrheaTwo.sunkenEyes},${diarrheaTwo.int},${diarrheaTwo.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
