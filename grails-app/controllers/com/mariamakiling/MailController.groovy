package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

class MailController {

    def index() {
    }
	
	def send = {
		sendMail {
			to params.email
			from "mariamakilingwvsu@gmail.com"
			body "Thank you ${params.userName} for using grails mail"
		}
		render "Success"
	}
}