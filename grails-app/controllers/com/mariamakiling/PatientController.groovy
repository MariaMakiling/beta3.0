package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class PatientController {

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index() {
		render Patient.list() as JSON
	}

	def save() {
		def patient = new Patient(request.JSON)
		if(patient.validate()){
			patient.save()
			patient.assignRole("ROLE_PATIENT")
			render(["success":true] as JSON)
		} else {
			response.status = 500
			render(patient.errors as JSON)
		}
	}

	def show() {
		println(params.id)
		render Patient.get(params.id) as JSON
	}

	def update() {
		def patient = Patient.get(params.id)
		patient.setProperties(request.JSON)
		if(patient.validate()){
			patient.save(true)
			render(["success":true] as JSON)
		} else {
			response.status = 500
			render(patient.errors as JSON)
		}
	}

	def delete() {
		def hw = HealthWorker.get(2)
		def patient = Patient.get(params.id)
		println("watta")
		UserRole.remove(patient, Role.findByAuthority("ROLE_PATIENT"), true)
		hw.removeFromPatients(patient)
		hw.save(true)
		patient.delete(flush: true)
		render(["success":true] as JSON)
	}
}
