package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class ImmunizationOneController {

    def index() { 
    	render ImmunizationOne.list() as JSON
    }

    def save() {
    	def newImmunizationOne = new ImmunizationOne(request.JSON)
        def imciOneList = ImciOne.list()
        def imciOne = imciOneList.last()
        newImmunizationOne.imciOne = imciOne
        newImmunizationOne.save(failOnError:true)
        println request.JSON
        println newImmunizationOne
        render(['success': true] as JSON)
    }

    def show() {
    	def immunizationOne = ImmunizationOne.get(params.id)

    	render immunizationOne as JSON
    }

    def list(){
        def listAllImmunizationOne = ImmunizationOne.executeQuery("from ImmunizationOne")
        StringWriter outputWriter = new StringWriter()

        listAllImmunizationOne.each { immunizationOne ->
            outputWriter.write("bcg:${immunizationOne.bcg}, dpt1:${immunizationOne.dpt1}, opv1:${immunizationOne.opv1}, hepb1:${immunizationOne.hepb1}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
