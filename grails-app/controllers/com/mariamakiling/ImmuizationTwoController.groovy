package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class ImmuizationTwoController {

    def index() { 
    	render ImmunizationTwo.list() as JSON
    }

    def save() {
    	def newImmunizationTwo= new ImmunizationTwo(request.JSON)
    	newImmunizationTwo.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def immunizationTwo = ImmunizationTwo.get(params.id)

    	render immunizationTwo as JSON
    }
}
