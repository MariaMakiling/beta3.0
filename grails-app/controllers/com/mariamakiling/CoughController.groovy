package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class CoughController {

    def index() { 
    	render Cough.list() as JSON
    }

    def save() {
    	def newCough = new Cough(request.JSON)
        def imciTwoList = ImciTwo.list()
        def imciTwo = imciTwoList.last()
        newCough.imciTwo = imciTwo
        newCough.save(failOnError:true)
        println request.JSON
        println newCough
        render(['success': true] as JSON)
    }

    def show() {
    	def cough = Cough.get(params.id)

    	render cough as JSON
    }

    def list(){
        def listAllCough = Cough.executeQuery("from Cough")
        StringWriter outputWriter = new StringWriter()

        listAllCough.each { cough ->
            outputWriter.write("${cough.cough},${cough.duration},${cough.count},${cough.chestIndrawing},${cough.stridor},${cough.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
