package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class ImciOneController {

    def index() {
    	render ImciOne.list() as JSON
    }

    def save() {
        def newImciOne = new ImciOne(request.JSON)
        newImciOne.save(failOnError: true)
        println request.JSON
        render(['success': true] as JSON)

    }

    def show() {
    	def imciOne = ImciOne.get(params.id)

    	render imciOne as JSON
    }

    def list(){
        def listAllImciOne = ImciOne.executeQuery("from ImciOne")
        StringWriter outputWriter = new StringWriter()

        listAllImciOne.each { imciOne ->
            outputWriter.write("assessment:${imciOne.diarrheaOne.assessment}, assessment:${imciOne.bacterialInfection.assessment},lowWeight:${imciOne.weightProblem.lowWeight},thrush:${imciOne.weightProblem.thrush},assessment:${imciOne.foodIntake.assessment},chin:${imciOne.attachment.chin}, mouth:${imciOne.attachment.mouth},lip:${imciOne.attachment.lip},areola:${imciOne.attachment.areola},sucking:${imciOne.attachment.sucking}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
