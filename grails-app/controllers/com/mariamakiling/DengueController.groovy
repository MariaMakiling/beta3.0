package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class DengueController {

    def index() { 
    	render Dengue.list() as JSON
    }

    def save() {
    	def newDengue = new Dengue(request.JSON)
    	newDengue.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def dengue = Dengue.get(params.id)

    	render dengue as JSON
    }

    def list(){
        def listAllDengue = Dengue.executeQuery("from Dengue")
        StringWriter outputWriter = new StringWriter()

        listAllDengue.each { dengue ->
            outputWriter.write("${dengue.bleeding},${dengue.black},${dengue.abdominalPain},${dengue.vomiting},${dengue.bleedingNoseGums},${dengue.skinPetechiae},${dengue.coldExtremities},${dengue.capillaryRefill},${dengue.tourniqueTest},${dengue.assessment}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
