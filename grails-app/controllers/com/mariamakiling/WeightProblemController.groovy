package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class WeightProblemController {

	static allowedMethods = [save: "POST"]

    def index() { 
    	render WeightProblem.list as JSON
    }

    def save() {
    	def newWeightProblem = new WeightProblem(request.JSON)
        def imciOneList = ImciOne.list()
        def imciOne = imciOneList.last()
        newWeightProblem.imciOne = imciOne
        newWeightProblem.save(failOnError:true)
        println request.JSON
        println newWeightProblem
        render(['success': true] as JSON)
    }

    def show() {
    	def weightProblem = WeightProblem.get(params.id)

    	render weightProblem as JSON
    }

    def list(){
        def listAllWeightProblem = WeightProblem.executeQuery("from WeightProblem")
        StringWriter outputWriter = new StringWriter()

        listAllWeightProblem.each { weightProblem ->
            outputWriter.write("lowWeight:${weightProblem.lowWeight}, thrush:${weightProblem.thrush}\n")
        }

        withFormat{
            csv{
                render(contentType:'text/plain',text:outputWriter)
            }
        }
    }
}
