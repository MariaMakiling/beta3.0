package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class AdminController {

	def index() {
		render Admin.list() as JSON
	}

		def save() {
		def admin = new Admin(request.JSON)
		if(admin.validate()){
			admin.save()
			admin.assignRole("ROLE_ADMIN")
			render(["success":true] as JSON)
		} else {
			response.status = 500
			render(admin.errors as JSON)
		}
	}

	def show() {
		println(params.id)
		render Admin.get(params.id) as JSON
	}

	def update() {
		def admin = Admin.get(params.id)
		admin.setProperties(request.JSON)
		if(admin.validate()){
			admin.save()
			render(["success":true] as JSON)
		} else {
			response.status = 500
			render(admin.errors as JSON)
		}
	}

	def delete() {
		def admin = Admin.get(params.id)
		UserRole.remove(admin, Role.findByAuthority("ROLE_ADMIN"), true)
		admin.delete(flush: true)
		render(["success":true] as JSON)
	}
}
