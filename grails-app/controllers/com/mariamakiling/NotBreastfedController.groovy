package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class NotBreastfedController {

    def index() {
    	render NotBreastfed.list() as JSON
    }

    def save() {
    	def newNotBreastfed = new NotBreastfed(request.JSON)
    	newNotBreastfed.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def notBreastfed = NotBreastfed.get(params.id)

    	render notBreastfed as JSON
    }
}
